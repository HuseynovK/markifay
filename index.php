<?php
require_once('simple_html_dom.php');
$html = file_get_html('https://search.ipaustralia.gov.au/trademarks/search/result?s=c54dd9e5-33e5-4353-90c7-eef519c5e9c5#_5576');

$total_number = $html->find('h2.qa-count', 0)->innertext;
$array = [];
$i = 0;
for ($j = 0; $j < ceil($total_number / 100); $j++) {
    $html = file_get_html('https://search.ipaustralia.gov.au/trademarks/search/result?s=c54dd9e5-33e5-4353-90c7-eef519c5e9c5#_5576' . '&p=' . $j);
    foreach ($html->find('tbody') as $tbody) {
        $tr = $tbody->find('tr', 0);
        $array[$i] ['number'] = $tr->find('td.number', 0)->find('a', 0)->innertext;
        $array[$i]['details_page_url'] = $tr->find('td.number', 0)->find('a', 0)->attr['href'];
        if ($tr->find('td.image', 0)) {
            $array[$i]['logo_url'] = $tr->find('td.image', 0)->find('img', 0)->attr['src'];
        } else {
            $array[$i]['logo_url'] = null;
        }
        $array[$i]['name'] = $tr->find('td.words', 0)->innertext;
        $array[$i]['classes'] = $tr->find('td.classes', 0)->innertext;
        if ($tr->find('td.status', 0)->find('div', 0)) {
            $s = explode(':', $tr->find('td.status', 0)->find('div', 0)->find('span', 0)->innertext);
            $array[$i]['status1'] = $s[0];
            $array[$i]['status2'] = $s[1];
        } else {
            $array[$i]['status1'] = $tr->find('td.status', 0)->innertext;
            $array[$i]['status2'] = '';
        }
        $i++;
    }
}
print_r($array);
die();